﻿using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text;

namespace Tbox.Api.Filters;

public sealed class EasyTokenValidateFilter : IEndpointFilter
{
    private readonly ILogger<EasyTokenValidateFilter> _logger;
    private readonly IEncryptedAppSettings<AppSettings> _appSettings;

    public EasyTokenValidateFilter(ILogger<EasyTokenValidateFilter> logger, IEncryptedAppSettings<AppSettings> appSettings)
    {
        _logger = logger;
        _appSettings = appSettings;
    }

    public async ValueTask<object?> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
    {
        var vdVIN = "__"; //"JTHB11B1002049154";
        var vdOneId = "__";//"e44aaac8-c4a2-46b1-ab1a-045f1d52d4ad";

        if (!_appSettings.AppSettings.IsBypassTokenValidation)
        {
            var headers = context.HttpContext.Request.Headers;
            if (headers?.Any() == true)
            {
                var adminSecretVal = headers[Constants.HeaderName.REQUEST_SECRET_NAME].FirstOrDefault();
                var requestedVIN = headers[Constants.HeaderName.REQUEST_VIN_KEY_NAME].FirstOrDefault();

                if (adminSecretVal == Constants.REQUEST_ADMIN_SECRET_KEY)
                {
                    // secret mode
                    var (aTokenValidateResult, _, aValidVIN) = await LocalTokenValidating(_logger, adminSecretVal, string.Empty, requestedVIN);
                    vdVIN = aValidVIN;

                    if (!aTokenValidateResult)
                        return Results.Unauthorized();
                }
                else
                {
                    var tokenVal = headers[Constants.HeaderName.REQUEST_TOKEN_KEY_NANE].FirstOrDefault();
                    if (!string.IsNullOrEmpty(tokenVal))
                    {
                        if (_appSettings.AppSettings.IsCallingMCTokenCheck)
                        {
                            var checkerStatusCode = McTokenChecker(tokenVal, _appSettings.AppSettings.McAppId, _appSettings.AppSettings.McAppVersion);
                            if (checkerStatusCode != OK)
                            {
                                return checkerStatusCode switch
                                {
                                    Unauthorized => Results.Unauthorized(),
                                    Forbidden => Results.Problem(detail: "", statusCode: 403),
                                    _ => Results.BadRequest("token error")
                                };
                            }
                        }

                        var (tokenValidateResult, validOneId, validVIN) = await LocalTokenValidating(_logger, adminSecretVal, tokenVal, requestedVIN);
                        vdOneId = validOneId;
                        vdVIN = validVIN;
                        if (!tokenValidateResult)
                            return Results.Unauthorized();
                    }
                    else
                    {
                        return Results.Unauthorized();
                    }
                }
            }
            else
            {
                return Results.Problem(detail: "", statusCode: (int)Forbidden);
            }

        }

        context.HttpContext.Request.Headers.Add(Constants.HeaderName.VALID_VIN_KEY_NAME, vdVIN);
        context.HttpContext.Request.Headers.Add(Constants.HeaderName.VALID_ONEID_KEY_NAME, vdOneId.ToLower());
        return await next(context);
    }

    async Task<(bool isValid, string? validOneId, string? validVin)> LocalTokenValidating(ILogger<EasyTokenValidateFilter> logger, string? adminSecretVal, string tokenValue, string? requestedVin)
    {
        var isTokenValid = false;
        var validOneId = "[secret_mode]"; // secret mode cannot get oneid from here
        var validVin = requestedVin;

        if (adminSecretVal == Constants.REQUEST_ADMIN_SECRET_KEY)
        {
            isTokenValid = true;
        }
        else
        {
            try
            {
                validOneId = GetOneIdInsideToken(tokenValue);
                using var rsa = RSA.Create();

                var tokenValidator = _appSettings.AppSettings.TokenValidator;
                rsa.ImportRSAPublicKey(Convert.FromBase64String(tokenValidator.PublicKey), out _);

                isTokenValid= TokenDecode(logger, tokenValue, rsa);

            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(TokenValidateFilter)} | {nameof(LocalTokenValidating)} | Requested {nameof(tokenValue)}=>\n{tokenValue}\n | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            }
        }
        return (isTokenValid, validOneId, validVin);
    }

    static string GetOneIdInsideToken(string tokenValue)
    {
        var handler = new JwtSecurityTokenHandler();
        var jstk = handler.ReadJwtToken(tokenValue);
        var claims = jstk.Claims.ToArray();
        var oneIdInsideToken = claims[0].Value; // subject as oneId
        return oneIdInsideToken;
    }

    static bool TokenDecode(ILogger<EasyTokenValidateFilter> logger, string token, RSA rsa)
    {
        var result = false;
        try
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtAlgorithm algorithm = new RS256Algorithm(rsa);
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

            _ = decoder.Decode(token);

            result = true;
        }
        catch (TokenNotYetValidException)
        {
            logger.LogError($"{nameof(EasyTokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(TokenNotYetValidException)} | Token is not valid yet");
        }
        catch (TokenExpiredException)
        {
            logger.LogError($"{nameof(EasyTokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(TokenExpiredException)} | Token has expired");
        }
        catch (SignatureVerificationException)
        {
            logger.LogError($"{nameof(EasyTokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(SignatureVerificationException)} | Token has invalid signature");
        }

        return result;
    }

    System.Net.HttpStatusCode McTokenChecker(string tokenValue, string appIdValue, string appVersionValue)
    {
        var httpClient = new HttpClient(new SocketsHttpHandler());
        var req = new HttpRequestMessage
        {
            RequestUri = new Uri(_appSettings.AppSettings.McServiceUrl),
            Method = HttpMethod.Post,
            Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(new
            {
                Method = "GET",
                Route = "/api/token/check",
                Body = ""
            }), Encoding.UTF8, "application/json"),
        };

        SetHeaders(httpClient, tokenValue, appIdValue, appVersionValue);

        var checker = httpClient.SendAsync(req).Result;
        if (!checker.IsSuccessStatusCode)
        {
            return checker.StatusCode switch
            {
                Unauthorized => Unauthorized,
                Forbidden => Forbidden,
                _ => BadRequest
            };
        }
        return OK;
    }

    static void SetHeaders(HttpClient httpClient, string token, string appId, string appVersion)
    {
        const string Authorization_Header_Name = "Authorization";
        httpClient.DefaultRequestHeaders.Remove(Authorization_Header_Name);
        httpClient.DefaultRequestHeaders.Add(Authorization_Header_Name, !token.StartsWith("Bearer ") ? $"Bearer {token}" : $"{token}");

        const string App_Id_Name = "APP_ID";
        httpClient.DefaultRequestHeaders.Remove(App_Id_Name);
        httpClient.DefaultRequestHeaders.Add(App_Id_Name, appId);

        const string App_Version_Name = "APP_VERSION";
        httpClient.DefaultRequestHeaders.Remove(App_Version_Name);
        httpClient.DefaultRequestHeaders.Add(App_Version_Name, appVersion);
    }
}