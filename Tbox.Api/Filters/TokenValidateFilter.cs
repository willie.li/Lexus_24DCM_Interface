﻿using JWT;
using JWT.Algorithms;
using JWT.Exceptions;
using JWT.Serializers;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Cryptography;
using System.Text;

namespace Tbox.Api.Filters;

public sealed class TokenValidateFilter : IEndpointFilter
{
    private readonly ILogger<TokenValidateFilter> _logger;
    private readonly IEncryptedAppSettings<AppSettings> _appSettings;
    private readonly IDataAccessService _dataAccessService;

    public TokenValidateFilter(ILogger<TokenValidateFilter> logger, IEncryptedAppSettings<AppSettings> appSettings, IDataAccessService dataAccessService)
    {
        _logger = logger;
        _appSettings = appSettings;
        _dataAccessService = dataAccessService;
    }

    public async ValueTask<object?> InvokeAsync(EndpointFilterInvocationContext context, EndpointFilterDelegate next)
    {
        var vdVIN = "__"; //"JTHB11B1002049154";
        var vdOneId = "__";//"e44aaac8-c4a2-46b1-ab1a-045f1d52d4ad";

        if (!_appSettings.AppSettings.IsBypassTokenValidation)
        {
            var headers = context.HttpContext.Request.Headers;
            if (headers?.Any() == true)
            {
                var adminSecretVal = headers[Constants.HeaderName.REQUEST_SECRET_NAME].FirstOrDefault();
                var requestedVIN = headers[Constants.HeaderName.REQUEST_VIN_KEY_NAME].FirstOrDefault();

                if (adminSecretVal == Constants.REQUEST_ADMIN_SECRET_KEY)
                {
                    // secret mode
                    var (aTokenValidateResult, _, aValidVIN) = await LocalTokenValidating(_logger, adminSecretVal, string.Empty, requestedVIN);
                    vdVIN = aValidVIN;

                    if (!aTokenValidateResult)
                        return Results.Unauthorized();
                }
                else
                {
                    var tokenVal = headers[Constants.HeaderName.REQUEST_TOKEN_KEY_NANE].FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(tokenVal))
                    {
                        if (_appSettings.AppSettings.IsCallingMCTokenCheck)
                        {
                            var checkerStatusCode = McTokenChecker(tokenVal, _appSettings.AppSettings.McAppId, _appSettings.AppSettings.McAppVersion);
                            if (checkerStatusCode != OK)
                            {
                                return checkerStatusCode switch
                                {
                                    Unauthorized => Results.Unauthorized(),
                                    Forbidden => Results.Problem(detail: "Forbidden", statusCode: 403),
                                    _ => Results.BadRequest("token error")
                                };
                            }
                        }

                        var (tokenValidateResult, validOneId, validVIN) = await LocalTokenValidating(_logger, adminSecretVal, tokenVal, requestedVIN);
                        vdOneId = validOneId;
                        vdVIN = validVIN;

                        if (!tokenValidateResult)
                            return Results.Unauthorized();
                    }
                    else
                    {
                        return Results.Unauthorized();
                    }
                }
            }
            else
            {
                return Results.Problem(detail: "Forbidden", statusCode: (int)Forbidden);
            }
        }

        context.HttpContext.Request.Headers.Add(Constants.HeaderName.VALID_VIN_KEY_NAME, vdVIN);
        context.HttpContext.Request.Headers.Add(Constants.HeaderName.VALID_ONEID_KEY_NAME, vdOneId);
        return await next(context);
    }

    async Task<(bool isValid, string? validOneId, string? validVin)> LocalTokenValidating(ILogger<TokenValidateFilter> logger, string? adminSecretVal, string tokenValue, string? requestedVin)
    {
        var isTokenValid = false;
        var validOneId = "[secret_mode]"; // secret mode cannot get oneid from here
        var validVin = requestedVin;

        if (adminSecretVal == Constants.REQUEST_ADMIN_SECRET_KEY)
        {
            isTokenValid = true;
        }
        else
        {
            try
            {
                validOneId = GetOneIdInsideToken(tokenValue);

                using var rsa = RSA.Create();

                var tokenValidator = _appSettings.AppSettings.TokenValidator;
                rsa.ImportRSAPublicKey(Convert.FromBase64String(tokenValidator.PublicKey), out _);

                // TODO: 「token: '[PII of type 'System.IdentityModel.Tokens.Jwt.JwtSecurityToken' is hidden.」coourred! need to be fixed!
                // https://community.auth0.com/t/idx10503-signature-validation-failed-token-does-not-have-a-kid/66885
                //_ = handler.ValidateToken(tokenValue, validationParameters, out _);

                var verifyResult = TokenDecode(logger, tokenValue ?? "", rsa);
                if (verifyResult)
                {
                    var isValidB2 = await _dataAccessService.VehicleInfoB2s.CheckIsValidUser(validOneId ?? string.Empty, requestedVin ?? string.Empty);
                    if (isValidB2)
                    {
                        isTokenValid = true;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{nameof(TokenValidateFilter)} | {nameof(LocalTokenValidating)} | Requested {nameof(tokenValue)}=>\n{tokenValue}\n | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            }
        }
        return (isTokenValid, validOneId, validVin);
    }

    static string GetOneIdInsideToken(string tokenValue)
    {
        var handler = new JwtSecurityTokenHandler();
        var jstk = handler.ReadJwtToken(tokenValue);
        var claims = jstk.Claims.ToArray();
        var oneIdInsideToken = claims[0].Value; // subject as oneId
        return oneIdInsideToken;
    }

    static bool TokenDecode(ILogger<TokenValidateFilter> logger, string token, RSA rsa)
    {
        var result = false;
        try
        {
            IJsonSerializer serializer = new JsonNetSerializer();
            IDateTimeProvider provider = new UtcDateTimeProvider();
            IJwtValidator validator = new JwtValidator(serializer, provider);
            IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
            IJwtAlgorithm algorithm = new RS256Algorithm(rsa);
            IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder, algorithm);

            _ = decoder.Decode(token);

            result = true;
        }
        catch (TokenNotYetValidException)
        {
            logger.LogError($"{nameof(TokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(TokenNotYetValidException)} | Token is not valid yet");
        }
        catch (TokenExpiredException)
        {
            logger.LogError($"{nameof(TokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(TokenExpiredException)} | Token has expired");
        }
        catch (SignatureVerificationException)
        {
            logger.LogError($"{nameof(TokenValidateFilter)} | {nameof(TokenDecode)} | {nameof(SignatureVerificationException)} | Token has invalid signature");
        }

        return result;
    }

    //static bool JwtSignatureVerify(string token, string key)
    //{
    //    var parts = token.Split('.');
    //    var header = parts[0];
    //    var payload = parts[1];
    //    byte[] crypto = Base64UrlDecode(parts[2]);

    //    string headerJson = Encoding.UTF8.GetString(Base64UrlDecode(header));
    //    JObject headerData = JObject.Parse(headerJson);

    //    string payloadJson = Encoding.UTF8.GetString(Base64UrlDecode(payload));
    //    JObject payloadData = JObject.Parse(payloadJson);

    //    var keyBytes = Convert.FromBase64String(key); // your key here

    //    AsymmetricKeyParameter asymmetricKeyParameter = PublicKeyFactory.CreateKey(keyBytes);
    //    RsaKeyParameters rsaKeyParameters = (RsaKeyParameters)asymmetricKeyParameter;
    //    RSAParameters rsaParameters = new RSAParameters();
    //    rsaParameters.Modulus = rsaKeyParameters.Modulus.ToByteArrayUnsigned();
    //    rsaParameters.Exponent = rsaKeyParameters.Exponent.ToByteArrayUnsigned();
    //    RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
    //    rsa.ImportParameters(rsaParameters);

    //    SHA256 sha256 = SHA256.Create();
    //    byte[] hash = sha256.ComputeHash(Encoding.UTF8.GetBytes(parts[0] + '.' + parts[1]));

    //    var rsaDeformatter = new RSAPKCS1SignatureDeformatter(rsa);
    //    rsaDeformatter.SetHashAlgorithm("SHA256");

    //    if (!rsaDeformatter.VerifySignature(hash, FromBase64Url(parts[2])))
    //        return false;

    //    return true;
    //}

    System.Net.HttpStatusCode McTokenChecker(string tokenValue, string appIdValue, string appVersionValue)
    {
        var httpClient = new HttpClient(new SocketsHttpHandler());
        var req = new HttpRequestMessage
        {
            RequestUri = new Uri(_appSettings.AppSettings.McServiceUrl),
            Method = HttpMethod.Post,
            Content = new StringContent(System.Text.Json.JsonSerializer.Serialize(new
            {
                Method = "GET",
                Route = "/api/token/check",
                Body = ""
            }), Encoding.UTF8, "application/json"),
        };

        SetHeaders(httpClient, tokenValue, appIdValue, appVersionValue);

        var checker = httpClient.SendAsync(req).Result;
        if (!checker.IsSuccessStatusCode)
        {
            return checker.StatusCode switch
            {
                Unauthorized => Unauthorized,
                Forbidden => Forbidden,
                _ => BadRequest
            };
        }
        return OK;
    }

    static void SetHeaders(HttpClient httpClient, string token, string appId, string appVersion)
    {
        const string Authorization_Header_Name = "Authorization";
        httpClient.DefaultRequestHeaders.Remove(Authorization_Header_Name);
        httpClient.DefaultRequestHeaders.Add(Authorization_Header_Name, !token.StartsWith("Bearer ") ? $"Bearer {token}" : $"{token}");

        const string App_Id_Name = "APP_ID";
        httpClient.DefaultRequestHeaders.Remove(App_Id_Name);
        httpClient.DefaultRequestHeaders.Add(App_Id_Name, appId);

        const string App_Version_Name = "APP_VERSION";
        httpClient.DefaultRequestHeaders.Remove(App_Version_Name);
        httpClient.DefaultRequestHeaders.Add(App_Version_Name, appVersion);
    }
}