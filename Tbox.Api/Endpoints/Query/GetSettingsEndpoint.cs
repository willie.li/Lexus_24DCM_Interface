﻿namespace Tbox.Api.Endpoints.Query;

public sealed class GetSettingsEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.QueryEndpointsPrefix}/GetSettings/{{vin}}/{{oneId}}", GetSettings)
            .WithTags(Constants.QueryTagName)
            .WithName(nameof(GetSettings))
            .WithDisplayName("查詢車況開關")
            .WithSummary("查詢車況開關")
            .WithDescription("APP 請不要使用此 Api")
            .WithOpenApi();
    }

    static async Task<IResult> GetSettings([FromServices] IDataAccessService dataAccessService, [FromRoute] string vin, [FromRoute] string oneId)
    {
        //var data = await dataAccessService.TBoxNotificationSettings.GetSettings(vin, oneId);
        //return Results.Ok(data);
        await Task.CompletedTask;
        return Results.NotFound();
    }
}