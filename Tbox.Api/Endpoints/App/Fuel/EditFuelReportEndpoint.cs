﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class EditFuelReportEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPut($"{Constants.AppEndpointsPrefix}/fuel", EditFuelReport)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(EditFuelReport))
            .WithDisplayName("編輯加油記錄")
            .WithSummary("編輯加油記錄")
            .WithDescription(Constants.REQUIRED_HEADERS_HINT)
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> EditFuelReport(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromBody] EditFuelReportRequestModel req)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        req ??= new EditFuelReportRequestModel();
        req.OneId = validOneId;
        req.VIN = validVIN;

        if (string.IsNullOrEmpty(req?.Id))
        {
            return Results.BadRequest("missing parameters");
        }
        _ = await dataAccessService.FuelReports.Edit(req);
        await dataAccessService.FuelSettings.AddOrUpdate(req.VIN, req.OneId, req.FuelType);

        return Results.Ok();
    }
}