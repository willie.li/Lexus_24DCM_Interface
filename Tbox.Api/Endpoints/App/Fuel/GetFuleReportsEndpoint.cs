﻿namespace Tbox.Api.Endpoints.App.Fuel;

public sealed class GetFuleReportsEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.AppEndpointsPrefix}/fuel/{{vin}}/{{oneId}}", GetFuelReports)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(GetFuelReports))
            .WithDisplayName("取得加油記錄")
            .WithSummary("取得加油記錄")
            .WithDescription(Constants.REQUIRED_HEADERS_HINT)
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetFuelReports(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin,
        [FromRoute] string oneId,
        [FromQuery] int p = 1, [FromQuery] int max = 100)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        if (p <= 0) p = 1;
        if (max <= 0 || max > 100) max = 100;

        vin = validVIN;

        var agreedTime = await dataAccessService.VehicleInfoB2s.FindAgreedTime(vin, oneId);
        var (setting, price) = await dataAccessService.FuelPrices.GetPriceWithSetting(vin, oneId);
        var data = dataAccessService.FuelReports.Get(vin, ((DateTimeOffset)agreedTime).ToUnixTimeMilliseconds()).Where(o => o.CreatedTime.HasValue).ToArray();
        var pager = data.Skip((p - 1) * max).Take(max);
        var curr = data.Where(o => o.CreatedTime.HasValue && o.CreatedTime.Value.Year >= DateTime.Now.Year).ToArray();    
        var ret = new FuelReportResponseModel
        {
            TotalCounts = curr.Length,
            FuelType = setting,
            FuelPrice = Math.Round(price, 0, MidpointRounding.AwayFromZero),
            TotalFuelPrice = GetFuelSummary(price, curr),
            TotalTravelDistance = curr.Sum(o => o.TravelDistance),
            Detail = pager.Select(o => new FuelDetailModel
            {
                Id = o.PermanentId,
                VIN = o.VIN,
                Epoch = o.Epoch,
                AvgFuelConsum = o.AvgFuelConsum,
                FuelConsumption = o.FuelConsumption,
                TravelDistance = o.TravelDistance,
                FuelDiff = o.UserFuelDiff ?? o.FuelDiff,
                Lat = o.Lat,
                Lon = o.Lon,
                Amount = ComputeFuelAmount(price, o)
            }),
            CurrentPage = p,
            MaxItemsPerPage = max
        };
        return Results.Ok(ret);
    }

    static double ComputeFuelAmount(double fuelPrice, FuelReport o)
    {
        var amount = o.UserAmount.HasValue == true
            ? (double)o.UserAmount.Value
            : o.UserFuelDiff.HasValue == true
                ? fuelPrice * o.UserFuelDiff.Value
                : fuelPrice * o.FuelDiff;

        return Math.Round(amount, 0, MidpointRounding.AwayFromZero);
    }

    static double GetFuelSummary(double fuelPrice, IEnumerable<FuelReport> data)
    {
        var summary = 0d;
        foreach (var r in CollectionsMarshal.AsSpan<FuelReport>(data.ToList()))
        {
            summary += ComputeFuelAmount(fuelPrice, r);
        }
        return summary;
    }
}