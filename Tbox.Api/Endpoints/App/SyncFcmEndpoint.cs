﻿namespace Tbox.Api.Endpoints.App;

public sealed class SyncFcmEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPost($"{Constants.AppEndpointsPrefix}/sync-fcm", SyncFcm)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(SyncFcm))
            .WithDisplayName("HT FCM RegisterIf 註冊")
            .WithSummary("HT FCM RegisterIf 註冊")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<EasyTokenValidateFilter>();
    }

    static IResult SyncFcm(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromHeader] string? token,
        [FromBody] FcmInfoRequestModel req)
    {
        if (string.IsNullOrEmpty(req?.OneId))
            return Results.BadRequest("missing parameter");

        if ((req?.AppId ?? 0) <= 0 || (req?.RegisterId ?? 0) <= 0)
            return Results.BadRequest("bad parameters");

        var added = dataAccessService.FcmInfos.Add(req);

        return Results.Ok(added);
    }
}