﻿using JWT.Exceptions;

namespace Tbox.Api.Endpoints.App;

public sealed class GetCarNameEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.AppEndpointsPrefix}/GetCarName/{{vin}}/{{oneId}}", GetCarName)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(GetCarName))
            .WithDisplayName("取得 CARNM 車型名稱、及綁定狀態")
            .WithSummary("由 VIN 碼取得 CARNM 車型名稱、及綁定狀態")
            .WithDescription(@$"使用 <strong>VIN</strong> 碼取得<br>CARNM 車型名稱、<br>及綁定狀態{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<EasyTokenValidateFilter>();
    }

    static IResult GetCarName(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromServices] ILogger<GetCarNameEndpoint> logger,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute(Name = "oneId")] string oneId)
    {
         
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();
        if (secret == null)
        {
            if (!validVIN.Equals(vin) || !validOneId.Equals(oneId))
            {
                logger.LogError($"{nameof(GetCarNameEndpoint)}| VIN:{validVIN}/{vin}, ONEID:{validOneId}/{oneId}");
                return Results.Problem(detail: "vin, oneId 不一致", statusCode: 403);
            }
        }
        var (carName, tosSTS, isCarOwner, tosOneId, isDemoCar) = dataAccessService.VehicleInfoAs.GetCarName(validVIN, secret == null? validOneId: oneId);

        carName = carName.Replace(" ", "");

        return Results.Ok(new
        {
            VIN = validVIN,
            CurrentOneId = oneId,
            TOSOneId = tosOneId,
            CarNM = carName,
            TOSSTS = tosSTS,
            IsCarOwner = isCarOwner,
            IsDemoCar = isDemoCar
        });
    }
}