﻿namespace Tbox.Api.Endpoints.App;

public sealed class SetVersionAgreementEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPut($"{Constants.AppEndpointsPrefix}/SetVersionAgreement", SetVersionAgreement)
            .WithTags(Constants.AppTagName)
            .WithName(nameof(SetVersionAgreement))
            .WithDisplayName("使用者操作 Tbox 前，同意使用者條款")
            .WithSummary("App 使用者操作 Tbox 前，同意使用者條款")
            .WithDescription($"IsAgree = true 同意條款，<br/>IsAgree != true 則徹銷同意條款{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<EasyTokenValidateFilter>();
    }

    static async Task<IResult> SetVersionAgreement(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService,
        [FromServices] IHttpContextAccessor httpContextAccessor,
        [FromServices] ILogger<SetVersionAgreementEndpoint> logger,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromBody] SetVersionAgreementRequestModel req)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        if (req is null)
        {
            return Results.BadRequest("missing parameters");
        }

#if RELEASE
        if (string.IsNullOrEmpty(req.IP))
        {
            req.IP = Strings.TryGetClientIP(httpContextAccessor);
        }
#endif
        //驗證 body vin & oneid equals to headers vin & oneid
        if (req.VIN != validVIN || req.OneId != validOneId)
        {
            logger.LogInformation($"req.VIN :{req.VIN} | validVIN :{validVIN} | req.OneId :{req.OneId} | validOneId :{validOneId}");
            return Results.Unauthorized();
        }
        req.VIN = validVIN;
        //req.OneId = validOneId;
        
        var ret = await dataAccessService.VehicleInfoB2s.SetVersionAgreement(req);
        if (ret == null) { return Results.Problem(detail: "Query result is null", statusCode: 400);}

        return Results.Ok(ret);
    }
}