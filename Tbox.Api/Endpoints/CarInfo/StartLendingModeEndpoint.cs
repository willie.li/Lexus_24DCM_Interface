﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class StartLendingModeEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapPost($"{Constants.CarInfoEndpointsPrefix}/start-lending/{{vin}}", StartLendingMode)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(StartLendingMode))
            .WithDisplayName("開啟借車模式")
            .WithSummary("實作 CMX11:開啟借車模式")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> StartLendingMode(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromBody] StartLendingModeRequestModel req)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var (status, result) = await carInfoService.StartLendingMode(validVIN, req);
        return status
            ? Results.Ok()
            : Results.BadRequest(result);
    }
}