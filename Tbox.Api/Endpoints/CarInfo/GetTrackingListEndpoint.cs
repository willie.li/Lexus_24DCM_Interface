﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetTrackingListEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/tracking-list/{{vin}}/{{epochstart:long}}/{{epochend:long}}", GetTrackingList)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetTrackingList))
            .WithDisplayName("查詢車輛異常移位列表")
            .WithSummary("實作 CMX05:查詢車輛異常移位列表")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetTrackingList(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] long epochstart, [FromRoute] long epochend)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.UtcNow;
        var ret = await carInfoService.GetTrackingReport(validVIN, epochstart, epochend);
        return Results.Ok(new ResultContainer<TrackingReportModel>
        {
            Result = ret,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }
}