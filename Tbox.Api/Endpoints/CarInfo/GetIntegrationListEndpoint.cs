﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetIntegrationListEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/lists/{{vin}}/{{epochstart:long}}/{{epochend:long}}", GetIntegrationList)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetIntegrationList))
            .WithDisplayName("整合查詢（正常異常）")
            .WithSummary("實作 CMX03456：整合查詢（正常異常）")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetIntegrationList(HttpRequest httpRequest,
        [FromServices] IDataAccessService dataAccessService, [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        string vin, long epochstart, long epochend)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;

        epochstart = await dataAccessService.VehicleInfoB2s.EpochstartRedefine(validVIN, epochstart);

        List<IntegrationObjectModel> tracks = new();

        // call cmx03
        var driveReport = await carInfoService.GetDriveReportList(validVIN, epochstart, epochend);
        await ProceedCMX03(tracks, driveReport, validVIN, carInfoService);

        // call cmx05
        await ProceedCMX05Data(tracks, validVIN, epochstart, epochend, carInfoService);

        var orderedTracks = tracks.OrderByDescending(x => x.TrackId).ToArray();

        return Results.Ok(new ResultContainer<IEnumerable<IntegrationObjectModel>>
        {
            Result = orderedTracks,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }

    static async Task ProceedCMX03(List<IntegrationObjectModel> tracks, DriveReportModel? driveReport, string vin, ICarInfoService carInfoService)
    {
        if (driveReport?.Reports?.Any() == true)
        {
            foreach (var row in driveReport.Reports)
            {
                if (row is not null)
                {
                    await ProceedCMX03Lopper(tracks, row, vin, carInfoService);
                }
            }
        }
    }

    static async Task ProceedCMX03Lopper(List<IntegrationObjectModel> tracks, ReportDetailModel row, string vin, ICarInfoService carInfoService)
    {
        var data = new IntegrationObjectModel
        {
            TrackId = row.TrackId,
            EpochStart = row.EpochStart,
            EpochEnd = row.EpochEnd,
            TravelDistance = row.TravelDistance,
            AvgFuelConsumption = row.AvgFuelConsumption / 10,
            EvPercentage = row.EvPercentage
        };

        data.TravelTime = Utilities.ComputeTravelTimeAsHM(data.EpochStart, data.EpochEnd);

        var speed = (double)data.TravelDistance / 10 / ((double)(data.EpochEnd - data.EpochStart) / 3600);
        data.AvgSpeed = (int)speed;

        // call cmx04 from each trackid
        var trackDetailsData = await carInfoService.GetDriveReportInfo(vin, row.TrackId);
        if (trackDetailsData?.Any() == true)
        {
            var trackDetails = trackDetailsData.Where(x => x.Lat > 0 && x.Lon > 0);
            if (trackDetails?.Any() == true)
            {
                foreach (var td in trackDetails)
                {
                    if (string.IsNullOrEmpty(td.Id)) td.Id = string.Empty;
                    if (string.IsNullOrEmpty(td.Vin)) td.Vin = string.Empty;
                    td.Lat /= 1000000; // 25.060274,
                    td.Lon /= 1000000; // 121.457728,
                }

                data.HasRegularResults = true;
                data.RegularResults = trackDetails.OrderBy(x => x.Epoch);
            }
        }
        tracks.Add(data);
    }

    static async Task ProceedCMX05Data(List<IntegrationObjectModel> tracks, string vin, long epochstart, long epochend, ICarInfoService carInfoService)
    {
        var dataCMX05 = await carInfoService.GetTrackingReport(vin, epochstart, epochend);
        if (dataCMX05?.Reports?.Any() == true)
        {
            foreach (var row in dataCMX05.Reports)
            {
                if (row is not null)
                {
                    await ProceedCMX05DataLopper(tracks, vin, row, carInfoService);
                }
            }
        }
    }

    static async Task ProceedCMX05DataLopper(List<IntegrationObjectModel> tracks, string vin, TrackingData row, ICarInfoService carInfoService)
    {
        var data = new IntegrationObjectModel
        {
            TrackId = row.TrackId,
            EpochStart = row.EpochStart,
            EpochEnd = row.EpochEnd,

            TravelDistance = -1,
            AvgFuelConsumption = -1,
            AvgSpeed = -1,
            EvPercentage = -1
        };

        data.TravelTime = Utilities.ComputeTravelTimeAsHM(data.EpochStart, data.EpochEnd);

        // call cmx06 from each trackid
        var trackDetails = await carInfoService.GetTrackingInfo(vin, row.TrackId);
        if (trackDetails?.Any() == true)
        {
            trackDetails = trackDetails.Where(x => x.Lat > 0 && x.Lon > 0);

            foreach (var td in trackDetails)
            {
                if (string.IsNullOrEmpty(td.Id)) td.Id = string.Empty;
                if (string.IsNullOrEmpty(td.Vin)) td.Vin = string.Empty;
                td.Lat /= 1000000; // 25.060274,
                td.Lon /= 1000000; // 121.457728,
            }

            data.HasAbnormalResults = true;
            data.AbnormalResults = trackDetails.OrderBy(x => x.Epoch);

            // data.AbnormalResults has 2+
            var abnormalMovingDistance = 0d;
            if (data.AbnormalResults.Count() >= 2)
            {
                var first = data.AbnormalResults.First();
                var last = data.AbnormalResults.Last();
                if (first is not null && last is not null)
                {
                    abnormalMovingDistance = Utilities.CalculateDistance(((double)first.Lat, (double)first.Lon), ((double)last.Lat, (double)last.Lon));
                }
            }
            data.AbnormalMovingDistance = abnormalMovingDistance;
        }
        tracks.Add(data);
    }
}