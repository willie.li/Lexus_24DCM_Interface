﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class GetCANInfoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.CarInfoEndpointsPrefix}/can/{{vin}}", GetCANInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(GetCANInfo))
            .WithDisplayName("查詢車輛CAN狀態")
            .WithSummary("實作 CMX02:查詢車輛CAN狀態")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> GetCANInfo(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        var t1 = DateTime.Now;

        var ret = await carInfoService.GetCanInfo(validVIN);

        return Results.Ok(new ResultContainer<CanInfoModel?>
        {
            Result = ret,
            Duration = (DateTime.Now - t1).TotalMilliseconds
        });
    }
}