﻿namespace Tbox.Api.Endpoints.CarInfo;

public sealed class DeleteTrackingInfoEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapDelete($"{Constants.CarInfoEndpointsPrefix}/tracking-info/{{vin}}/{{trackId:long}}", DeleteTrackingInfo)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(DeleteTrackingInfo))
            .WithDisplayName("刪除車輛異常移位內容")
            .WithSummary("實作 CMX06-2:刪除車輛異常移位內容")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();

        app.MapPost($"{Constants.CarInfoEndpointsPrefix}/tracking-info", DeleteTrackingInfos)
            .WithTags(Constants.CarInfoTagName)
            .WithName(nameof(DeleteTrackingInfos))
            .WithDisplayName("刪除車輛異常移位內容（多筆）")
            .WithSummary("實作 CMX06-2:刪除車輛異常移位內容（多筆）")
            .WithDescription($"{Constants.REQUIRED_HEADERS_HINT}")
            .WithOpenApi()
            .AddEndpointFilter<AppEntryFilter>()
            .AddEndpointFilter<TokenValidateFilter>();
    }

    static async Task<IResult> DeleteTrackingInfo(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromRoute] string vin, [FromRoute] long trackId)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        await carInfoService.DeleteTrackingInfo(validVIN, trackId);
        return Results.Ok();
    }

    static async Task<IResult> DeleteTrackingInfos(HttpRequest httpRequest,
        [FromServices] ICarInfoService carInfoService,
        [FromHeader] string? token,
        [FromHeader] string? requestedVIN,
        [FromHeader] string? secret,
        [FromBody] IEnumerable<DeletingRequestDataModel> data)
    {
        var (validOneId, validVIN) = Utilities.TryGetValidDataFromHeader(httpRequest);
        if (string.IsNullOrEmpty(validOneId) || string.IsNullOrEmpty(validVIN))
            return Results.Unauthorized();

        if (data?.Any() == true)
        {
            foreach (var q in data)
            {
                await carInfoService.DeleteTrackingInfo(validVIN, q.TrackId);
            }
        }
        return Results.Ok();
    }
}