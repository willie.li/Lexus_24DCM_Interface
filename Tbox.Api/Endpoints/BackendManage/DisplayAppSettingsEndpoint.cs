﻿namespace Tbox.Api.Endpoints.BackendManage;

public sealed class DisplayAppSettingsEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet("/vault", AppSettingsDisplayer)
            .WithTags(Constants.BackendManageTagName)
            .WithName(nameof(AppSettingsDisplayer))
            .WithDisplayName("AppSettingsDisplayer")
            .WithSummary("AppSettingsDisplayer")
#if DEBUG
            .WithOpenApi();
#else
            .ExcludeFromDescription();
#endif
    }

    static IResult AppSettingsDisplayer([FromServices] ILogger<Program> logger,
            [FromServices] IEncryptedAppSettings<AppSettings> appSettings,
            [FromServices] IHttpContextAccessor httpContextAccessor)
    {
        if (!Strings.CheckAccessable(httpContextAccessor, logger))
            return Results.NotFound();

        var settings = appSettings?.AppSettings;

        //RC.Default[Constants.TboxSqlConnectionStringKeyName] = appSettings.AppSettings.TboxDbConnection;

        return Results.Ok();
    }
}