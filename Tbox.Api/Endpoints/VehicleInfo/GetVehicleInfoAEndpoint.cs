﻿using Microsoft.Data.SqlClient;
using System.Net.Sockets;
using System.Net;
using Tbox.Api.Endpoints.App;
using System.Security.Cryptography;

namespace Tbox.Api.Endpoints.VehicleInfo;

public sealed class GetVehicleInfoAEndpoint : IEndpointDefinition
{
    public void DefineEndPoints(WebApplication app)
    {
        app.MapGet($"{Constants.VehicleInfoEndpointsPrefix}/get_vehicle_info_a/{{obd2vin}}", GetVehicleInfoA)
            .WithTags(Constants.VehicleInfoTagName)
            .WithName(nameof(GetVehicleInfoA))
            .WithDisplayName("查詢車籍資料")
            .WithSummary("實作 HT01-A：查詢車籍資料")
            .WithDescription("直接查詢 ECHO.MCLUB/Lexus Server，<br/>順便存 IoV Flag")
            .WithOpenApi()
            .AddEndpointFilter<CmxEntryFilter>();
    }

    static async Task<IResult> GetVehicleInfoA([FromServices] IHttpContextAccessor httpContextAccessor,
        [FromServices] IEncryptedAppSettings<AppSettings> settings, [FromServices] IDataAccessService dataAccessService,
        [FromRoute] string obd2vin, [FromServices] ILogger<GetVehicleInfoAEndpoint> logger)
    {
        var t1 = DateTime.Now;

        var data = await dataAccessService.VehicleInfoAs.GetVehicleInfoA(obd2vin);
        if (data is null)
            return Results.BadRequest($"{obd2vin} data not found.");

        using var mclubConn = new SqlConnection(settings.AppSettings.MclubDbConnection);
        var mclubService = new MclubService(mclubConn);
        var mclubresp = await mclubService.MarkIoVCarType(data.VIN); //接收MCLUB錯誤訊息

        logger.LogInformation($"{nameof(GetVehicleInfoAEndpoint)}| mclubsp:{mclubresp} "); // If mclubresp ==0，sp執行成功 else mclubresp ==1，sp執行失敗。
        var clientIp = httpContextAccessor.HttpContext?.Connection?.RemoteIpAddress?.ToString();



        var headers = httpContextAccessor.HttpContext?.Request.Headers;
        var authorizedUser = clientIp;
        var remoteIpAddress = clientIp;
        //if (headers?.Any() == true)
        //{
        //    authorizedUser = headers["AuthorizedUser"].FirstOrDefault();
        //    remoteIpAddress = headers["RemoteIpAddress"].FirstOrDefault();
        //}

        var obd2vinTemp = await dataAccessService.VehicleVinRefs.Get(obd2vin);
        if (obd2vinTemp is null)
        {
            _ = dataAccessService.VehicleVinRefs.Add(new VehicleVinRef
            {
                OBD2VIN = obd2vin,
                VIN = data.VIN,
                CRTPGMID = clientIp ?? "SQLInsert",
                MTPGMID = clientIp ?? "SQLInsert",
                CRTDT = DateTime.Now,
                MTDT = DateTime.Now,

            });
        }
        var result = new BaseOutputModel<dynamic>
        {
            Durations = (DateTime.Now - t1).TotalMilliseconds,
            Data = new
            {
                CARNM = data.CARNM?.Trim(),
                TMCMDL = data.TMCMDL?.Trim(),
                CARMDL = data.CARMDL?.Trim(),
                SFX = data.SFX?.Trim(),
                MDLYEAR = data.MDLYEAR,
                FRAN = data.FRAN?.Trim(),
            }
        };
        mclubConn.Close();
        if (mclubresp == 1)
        {
            return Results.Ok();
        }
        return Results.Ok(result);
    }

}