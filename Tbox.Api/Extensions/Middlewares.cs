﻿namespace Tbox.Api.Extensions;

public static class Middlewares
{
    public static void AddServices(this WebApplicationBuilder builder)
    {
        builder.Host.UseSerilog();
        Log.Logger = new LoggerConfiguration().ReadFrom.Configuration(builder.Configuration).CreateLogger();

        var services = builder.Services;

        services.AddDbContext<ApplicationDbContext>();

        services.AddSingleton<IHttpClientWrapper, HttpClientWrapper>();
        services.AddSingleton<IEncryptedAppSettings<AppSettings>, EncryptedAppSettings<AppSettings>>();
        services.AddTransient<IResponseCompressionProvider, ResponseCompressionProvider>();
        services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
        services.AddTransient<IDataAccessService, DataAccessService>();
        services.AddTransient<ICarInfoService, CarInfoService>();

        services.AddResponseCompression();
        services.AddEndpointDefinitions(typeof(Program));
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();
        services.AddRazorPages();

        services.AddW3CLogging(options =>
        {
            options.LoggingFields = W3CLoggingFields.All;
            options.FileSizeLimit = 5_242_880; //5 * 1024 * 1024;
            options.RetainedFileCountLimit = 4;
            options.FileName = "w3clogs";
            options.FlushInterval = TimeSpan.FromSeconds(60);
        });
    }

    public static void EnabledMiddlewares(this WebApplication app)
    {
        var scopeFactory = app.Services.GetRequiredService<IServiceScopeFactory>();
        using var scope = scopeFactory.CreateScope();

        var encryptedAppSettings = scope.ServiceProvider.GetService<IEncryptedAppSettings<AppSettings>>();
        var settings = (encryptedAppSettings?.AppSettings) ?? throw new Exception("misiing appSettings");

        // set db conn strin
        System.Runtime.Caching.MemoryCache.Default[Constants.TboxSqlConnectionStringKeyName] = settings.TboxDbConnection;

        // set db conn string in AppDomain
        AppDomain currentDomain = AppDomain.CurrentDomain;
        currentDomain.SetData(Constants.TboxSqlConnectionStringKeyName, $"{settings.TboxDbConnection}");

        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Error");
        }

        app.UseHttpLogging();
        app.UseW3CLogging();
        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.MapRazorPages();
        app.UseEndpointDefinitions();
        app.UseSwagger();
        app.UseSwaggerUI(opt =>
        {
#if RELEASE            
            opt.SwaggerEndpoint("/api-doc/swagger.json", "v1");
#endif

            //opt.InjectStylesheet("/css/swagger-dark.css");
            //opt.InjectStylesheet("/css/theme-outline.css");
            opt.InjectStylesheet("/css/theme-material.css");
        });
        app.UseResponseCompression();

#if DEBUG
        app.Urls.Add("https://localhost:5001/");
#endif

        app.MapGet("/", () =>
        {
            return Results.Ok("Hello TBox @.net 7");
        }).ExcludeFromDescription();
    }
}