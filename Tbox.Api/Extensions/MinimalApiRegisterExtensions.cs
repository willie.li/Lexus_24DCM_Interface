﻿namespace Microsoft.Extensions.DependencyInjection;

public static class EndpointDefinitionExtensions
{
    public static void AddEndpointDefinitions(this IServiceCollection services, params Type[] scanMarkers)
    {
        var t1 = DateTime.Now;

        List<IEndpointDefinition> list = new();
        foreach (Type type in scanMarkers)
        {
            list.AddRange(type.Assembly.ExportedTypes.Where((Type x) => typeof(IEndpointDefinition)!.IsAssignableFrom(x) && !x.IsAbstract).Select(new Func<Type, object>(Activator.CreateInstance)).Cast<IEndpointDefinition>());
        }
        services.AddSingleton((IReadOnlyCollection<IEndpointDefinition>)list);

        Console.WriteLine($"{nameof(AddEndpointDefinitions)} => {(DateTime.Now - t1).TotalMilliseconds} ms.");
    }

    public static void UseEndpointDefinitions(this WebApplication app)
    {
        var t1 = DateTime.Now;

        IReadOnlyCollection<IEndpointDefinition> requiredService = app.Services.GetRequiredService<IReadOnlyCollection<IEndpointDefinition>>();
        foreach (var endpoint in CollectionsMarshal.AsSpan<IEndpointDefinition>(requiredService.ToList()))
        {
            endpoint.DefineEndPoints(app);
        }

        Console.WriteLine($"{nameof(UseEndpointDefinitions)} => {(DateTime.Now - t1).TotalMilliseconds} ms.");
    }
}

public interface IEndpointDefinition
{
    void DefineEndPoints(WebApplication app);
}