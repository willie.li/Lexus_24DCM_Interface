﻿namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX12:關閉借車模式
    /// </summary>
    /// <param name="vin"></param>
    /// <returns></returns>
    public async Task<(bool, string)> EndLendingMode(string vin)
    {
        var req = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            RequestUri = new Uri($"{_baseUrl}/End_LendingMode/{vin}")
        };
        try
        {
            var Request = req;
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                return (true, string.Empty);
            }
            else
            {
                if (resp is not null)
                {
                    var content = await resp.Content.ReadAsStringAsync();
                    return (false, content);
                }
                return (false, "Something went wrong.");
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(EndLendingMode)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return (false, ex.Message);
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<(bool, string)> EndLendingMode(string vin);
}