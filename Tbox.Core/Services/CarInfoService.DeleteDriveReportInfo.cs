﻿namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX04-2:刪除車輛行駛報告內容
    /// </summary>
    /// <param name="vin"></param>
    /// <param name="trackId"></param>
    public async Task DeleteDriveReportInfo(string vin, long trackId)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Delete,
            RequestUri = new Uri($"{_baseUrl}/Del_DriveReport_Info?vin={vin}&trackid={trackId}")
        };
        try
        {
            _httpClient.Send(Request);
            await Task.CompletedTask;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(DeleteDriveReportInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            await Task.CompletedTask;
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task DeleteDriveReportInfo(string vin, long trackId);
}