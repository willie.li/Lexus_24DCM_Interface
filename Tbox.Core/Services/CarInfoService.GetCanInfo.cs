﻿using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX02:查詢車輛CAN狀態 api caller
    /// </summary>
    /// <param name="vin">引擎號碼</param>
    /// <returns>CanInfo</returns>
    public async Task<CanInfoModel?> GetCanInfo(string vin)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_CAN_Info/{vin}")
        };

        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<CanInfoModel>(content);
                    if (data is not null)
                    {
                        data.StartUpStatusName = ((StartupStatusEnum)data.StartUpStatus).ToString();

                        data.CarDoorLFStatusName = ((CarDoorTypeEnum)data.CarDoorLF).ToString();
                        data.CarDoorRFStatusName = ((CarDoorTypeEnum)data.CarDoorRF).ToString();
                        data.CarDoorLBStatusName = ((CarDoorTypeEnum)data.CarDoorLB).ToString();
                        data.CarDoorRBStatusName = ((CarDoorTypeEnum)data.CarDoorRB).ToString();
                        data.LuggageDoorStatusName = ((CarDoorTypeEnum)data.LuggageDoor).ToString();
                        data.HatchbackDoorStatusName = ((CarDoorTypeEnum)data.HatchbackDoor).ToString();

                        data.DoorLockLFStatusName = ((DoorLockTypeEnum)data.DoorLockLF).ToString();
                        data.DoorLockRFStatusName = ((DoorLockTypeEnum)data.DoorLockRF).ToString();
                        data.DoorLockLBStatusName = ((DoorLockTypeEnum)data.DoorLockLB).ToString();
                        data.DoorLockRBStatusName = ((DoorLockTypeEnum)data.DoorLockRB).ToString();

                        return data;
                    }
                }
            }
            return null;
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetCanInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return null;
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<CanInfoModel?> GetCanInfo(string vin);
}