﻿using Tbox.Core.Domains;
using Tbox.Core.Repositories;

namespace Tbox.Core.Services;

public class DataAccessService : IDataAccessService
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger<DataAccessService> _logger;
    private bool _disposedValue;

    public DataAccessService(ApplicationDbContext db, ILogger<DataAccessService> logger)
    {
        _db = db;
        _logger = logger;
    }

    private IVehicleInfoARepository? _vehicleInfoAs;
    public IVehicleInfoARepository VehicleInfoAs { get => _vehicleInfoAs ??= new VehicleInfoARepository(_db, _logger); }

    private IVehicleInfoB2Repository? _vehicleInfoB2s;
    public IVehicleInfoB2Repository VehicleInfoB2s { get => _vehicleInfoB2s ??= new VehicleInfoB2Repository(_db, _logger); }

    private IVehicleVinRefRepository? _vehicleVinRefs;
    public IVehicleVinRefRepository VehicleVinRefs { get => _vehicleVinRefs ??= new VehicleVinRefRepository(_db, _logger); }

    private ITBoxNotificationSettingRepository? _tboxNotificationSettings;
    public ITBoxNotificationSettingRepository TBoxNotificationSettings { get => _tboxNotificationSettings ??= new TBoxNotificationSettingRepository(_db, _logger); }

    private IFuelSettingRepository? _fuelSettings;
    public IFuelSettingRepository FuelSettings { get => _fuelSettings ??= new FuelSettingRepository(_db, _logger); }

    private IFuelPriceRepository? _fuelPrices;
    public IFuelPriceRepository FuelPrices { get => _fuelPrices ??= new FuelPriceRepository(_db, _logger); }

    private IFuelReportRepository? _fuelReports;
    public IFuelReportRepository FuelReports { get => _fuelReports ??= new FuelReportRepository(_db, _logger); }

    private IFcmInfoRepository? _fcmInfos;
    public IFcmInfoRepository FcmInfos { get => _fcmInfos ??= new FcmInfoRepository(_db, _logger); }

    public int Complete()
    {
        return _db.SaveChanges();
    }

    private void Dispose(bool disposing)
    {
        if (!_disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects)
            }

            // TODO: free unmanaged resources (unmanaged objects) and override finalizer
            // TODO: set large fields to null
            _disposedValue = true;
        }
    }

    ~DataAccessService()
    {
        Dispose(disposing: false);
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

public interface IDataAccessService : IDisposable
{
    IVehicleInfoARepository VehicleInfoAs { get; }
    IVehicleInfoB2Repository VehicleInfoB2s { get; }
    IVehicleVinRefRepository VehicleVinRefs { get; }
    ITBoxNotificationSettingRepository TBoxNotificationSettings { get; }
    IFuelSettingRepository FuelSettings { get; }
    IFuelPriceRepository FuelPrices { get; }
    IFuelReportRepository FuelReports { get; }
    IFcmInfoRepository FcmInfos { get; }

    int Complete();
}