﻿using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX05:查詢車輛異常移位列表
    /// </summary>
    /// <param name="vin">引擎號</param>
    /// <param name="epochStart">查詢日期區間 起</param>
    /// <param name="epochEnd">查詢日期區間 迄</param>
    /// <returns>TrackingReport</returns>
    public async Task<TrackingReportModel?> GetTrackingReport(string vin, long epochStart, long epochEnd)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/GetTracking_List/{vin}?epochstart={epochStart}&epochend={epochEnd}")
        };
        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<TrackingReportModel>(content);
                    return data;
                }
            }
            return new TrackingReportModel
            {
                Total = 0,
                Reports = null
            };
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetTrackingReport)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return new TrackingReportModel
            {
                Total = 0,
                Reports = null
            };
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<TrackingReportModel?> GetTrackingReport(string vin, long epochStart, long epochEnd);
}