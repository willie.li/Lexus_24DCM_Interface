﻿using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX15:取得借車模式狀態(0:off 1:親友借車 2:朋友借車 3:代客泊車)
    /// </summary>
    /// <param name="vin"></param>
    /// <returns></returns>
    public async Task<(bool status, LendingModeStatusModel results)> GetLendingMode(string vin)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_LendingMode_Status/{vin}")
        };
        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                var results  = JsonSerializer.Deserialize<LendingModeStatusModel>(content);
                return (true, results!= null? results: new LendingModeStatusModel());
            }
            else
            {
                return (false, new LendingModeStatusModel());
            }
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetLendingMode)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return (false, new LendingModeStatusModel());
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<(bool status, LendingModeStatusModel results)> GetLendingMode(string vin);
}