﻿using Dapper;
using Microsoft.Data.SqlClient;
using System.Data;
using Tbox.Core.Domains;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed class MclubService : IMclubService
{
    private readonly IDbConnection _sqlConnection;

    public MclubService(SqlConnection sqlConnection)
    {
        _sqlConnection = sqlConnection;
    }

    /// <summary>
    /// 註記IoV車種
    /// </summary>
    /// <param name="vin"></param>
    /// <returns></returns>
    public async Task<int> MarkIoVCarType(string vin)
    {
        // as test result, an un-exist vin, still returning a result string [IOV註記完成]
        var dynamicParameters = new DynamicParameters();
        dynamicParameters.Add("@O_CHR_MSGTYPE",string.Empty,DbType.String, direction: ParameterDirection.Output);
        dynamicParameters.Add("@O_INT_MSGLEVEL",0,DbType.Int16, direction: ParameterDirection.Output);
        dynamicParameters.Add("@O_CHR_MESSAGE",string.Empty,DbType.String, direction: ParameterDirection.Output);
        dynamicParameters.Add("I_CHR_VIN", vin ,DbType.String, direction: ParameterDirection.Input);

        var mclubresp=await _sqlConnection.ExecuteScalarAsync(
            "[USP_IOV0000_I01]",
            dynamicParameters,
            null,
            30,
            CommandType.StoredProcedure);
        var msgtype = dynamicParameters.Get<String>("@O_CHR_MSGTYPE");
        var reuslt = dynamicParameters.Get<String>("@O_CHR_MESSAGE");
        var status = dynamicParameters.Get<Int16>("@O_INT_MSGLEVEL");
        return status;
    }

    /// <summary>
    /// 取得交車資料
    /// </summary>
    /// <param name="vin"></param>
    /// <returns></returns>
    public async Task<MclubVehicleCache?> GetCarRetailData(string vin)
    {
        var ret = await _sqlConnection.QueryFirstOrDefaultAsync<MclubVehicleCache>(
            "USP_IOV0000_Q01",
            new { I_CHR_VIN = vin },
            null,
            30,
            CommandType.StoredProcedure);

        if (ret is not null)
        {
            var bdt = DateTime.TryParse(ret.REDLDT, out var dt);
            if (bdt)
            {
                ret.REDLDT_r = dt;
            }
        }
        return ret;
    }
}

public interface IMclubService
{
    Task<int> MarkIoVCarType(string vin);
    Task<MclubVehicleCache?> GetCarRetailData(string vin);
}