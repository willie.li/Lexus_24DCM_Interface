﻿using Azure.Core;
using System.Text.Json;
using Tbox.Core.Models;

namespace Tbox.Core.Services;

public sealed partial class CarInfoService
{
    /// <summary>
    /// CMX06:查詢車輛異常移位內容
    /// </summary>
    /// <param name="vin">vin</param>
    /// <param name="trackId">track id</param>
    /// <returns>IEnumerable<TrackingInfo></returns>
    public async Task<IEnumerable<TrackingInfoModel>?> GetTrackingInfo(string vin, long trackId)
    {
        var Request = new HttpRequestMessage
        {
            Method = HttpMethod.Get,
            RequestUri = new Uri($"{_baseUrl}/Get_Tracking_Info/{vin}/{trackId}")
        };
        try
        {
            _httpClient.Send(Request);
            using var resp = _httpClient.Response;
            if (resp?.IsSuccessStatusCode == true)
            {
                var content = await resp.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(content))
                {
                    var data = JsonSerializer.Deserialize<IEnumerable<TrackingInfoModel>>(content);
                    if (data?.Any() == true)
                    {
                        return data;
                    }
                }
            }
            return Array.Empty<TrackingInfoModel>();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(CarInfoService)} | {nameof(GetTrackingInfo)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return Array.Empty<TrackingInfoModel>();
#endif
        }
    }
}

public partial interface ICarInfoService
{
    Task<IEnumerable<TrackingInfoModel>?> GetTrackingInfo(string vin, long trackId);
}