﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.Extensions.Logging;
using Tbox.Core.Domains;
using Tbox.Core.Models.RequestModels;

namespace Tbox.Core.Repositories;

public sealed class FuelReportRepository : Repository<FuelReport>, IFuelReportRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public FuelReportRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<bool> Create(FuelReport data)
    {
        await _db.FuelReports.AddAsync(data);
        var result =await _db.SaveChangesAsync();
        return result > 0;
    }

    public IEnumerable<FuelReport> Get(string vin, long agreedTime)
    {
        try
        {
            return _db.FuelReports.Where(o => o.VIN == vin && o.Epoch * 1_000 > agreedTime && o.IsDeleted != true).ToArray();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelReportRepository)} | {nameof(Get)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return Array.Empty<FuelReport>();
#endif
        }
    }

    public async Task<int> Edit(EditFuelReportRequestModel data)
    {
        try
        {
            // var sql = "UPDATE [FuelReports] SET [UserAmount]=@amount, [UserFuelDiff]=@fuelDiff WHERE [VIN]=@vin AND [PermanentId]=@id;";
            var edited = await _db.FuelReports.FirstOrDefaultAsync(o => o.PermanentId == data.Id);
            if (edited is not null)
            {
                edited.UserAmount = (decimal)data.Amount;
                edited.UserFuelDiff = (int)data.FuelDiff;
                edited.ModifiedTime = DateTime.Now;
                _ = await _db.SaveChangesAsync();

                return 1;
            }
            return 0;
        }
        catch(Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelReportRepository)} | {nameof(Edit)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return -1;
#endif
        }
    }

    public async Task<int> Delete(string id)
    {
        try
        {
            var rpt = await _db.FuelReports.FirstOrDefaultAsync(o => o.PermanentId == id);
            if (rpt is not null)
            {
                rpt.IsDeleted = true;
                rpt.ModifiedTime = DateTime.Now;
                _ = await _db.SaveChangesAsync();

                return 1;
            }
            return 0;
        }
        catch(Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelReportRepository)} | {nameof(Delete)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return -1;
#endif
        }
    }

    public FuelReport GetCheckForFuelReport(string vin, long epoch)
    {
        try
        {
            return _db.FuelReports.FirstOrDefault(o => o.VIN == vin &&  o.Epoch == epoch && o.IsDeleted != true) ?? new FuelReport();
        }
        catch (Exception ex)
        {
#if DEBUG
            throw;
#else
            _logger.LogError($"{nameof(FuelReportRepository)} | {nameof(GetCheckForFuelReport)} | {ex.Message}\n{ex.InnerException?.Message}\n{ex.StackTrace}");
            return new FuelReport();
#endif
        }
    }

}
public interface IFuelReportRepository
{
    Task<bool> Create(FuelReport data);
    IEnumerable<FuelReport> Get(string vin, long agreedTime);
    Task<int> Edit(EditFuelReportRequestModel data);
    Task<int> Delete(string id);
    FuelReport GetCheckForFuelReport(string vin, long epoch);
}