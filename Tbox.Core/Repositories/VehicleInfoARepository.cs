﻿using Tbox.Core.Domains;

namespace Tbox.Core.Repositories;

public sealed class VehicleInfoARepository : Repository<VehicleInfoA>, IVehicleInfoARepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public VehicleInfoARepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<VehicleInfoA?> GetVehicleInfoA(string obd2vin)
    {
        var data = await _db.VehicleInfoAs.FirstOrDefaultAsync(o => o.VIN == obd2vin || o.BDNO == obd2vin);
        return data;
    }

    public (string carName, string tosSTS, bool isCarOwner, string? tosOneId, bool isDemoCar) GetCarName(string vin, string oneId)
    {
        var carName = _db.VehicleInfoAs.FirstOrDefault(o => o.VIN == vin)?.CARNM ?? string.Empty;

        var tosOneId = _db.VehicleInfoB2s.FirstOrDefault(o => o.VIN == vin && o.TOSSTS == "Y")?.ONEID.ToString().ToLower() ?? string.Empty;
        var tosSTS = "N";
        var isCarOwner = false;
        var isDemoCar = _db.VehicleInfoB2Demos.Any(o => o.VIN == vin);

        var term = _db.VehicleInfoB2s.FirstOrDefault(o => (o.VIN ?? string.Empty).ToUpper() == vin.ToUpper() && o.ONEID.ToString().ToLower() == tosOneId.ToLower());
        if (term is not null
            && term.ONEID.ToString().ToLower() == tosOneId.ToLower())
        {
            tosSTS = !string.IsNullOrWhiteSpace(tosOneId)?"Y": "N";
            isCarOwner = term.TOSSTS == "Y";
        }

        return (carName, tosSTS, isCarOwner, tosOneId, isDemoCar);
    }
}

public interface IVehicleInfoARepository
{
    Task<VehicleInfoA?> GetVehicleInfoA(string obd2vin);
    (string carName, string tosSTS, bool isCarOwner, string? tosOneId, bool isDemoCar) GetCarName(string vin, string oneId);
}