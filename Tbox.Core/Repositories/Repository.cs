﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Tbox.Core.Repositories;

public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class, new()
{
    protected readonly DbSet<TEntity> _entities;
    private readonly ILogger _logger;

    public Repository(DbContext context, ILogger logger)
    {
        _entities = context.Set<TEntity>();
        _logger = logger;
    }

    public IQueryable<TEntity> All()
    {
        return _entities;
    }

    public async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate)
    {
        return await _entities.Where(predicate).ToArrayAsync();
    }

    /// <summary>
    /// FirstOrDefault of data set
    /// </summary>
    public async Task<TEntity?> Single(Expression<Func<TEntity, bool>> predicate)
    {
        return await _entities.FirstOrDefaultAsync(predicate);
    }

    public void Add(TEntity entity)
    {
        _entities.Add(entity);
    }

    public async Task AddAsync(TEntity entity)
    {
        await _entities.AddAsync(entity);
    }

    public void AddRange(IEnumerable<TEntity> entities)
    {
        _entities.AddRange(entities);
    }

    public async Task AddRangeAsync(IEnumerable<TEntity> entities)
    {
        await _entities.AddRangeAsync(entities);
    }

    public void Remove(TEntity entity)
    {
        _entities.Remove(entity);
    }

    public void RemoveRange(IEnumerable<TEntity> entities)
    {
        _entities.RemoveRange(entities);
    }
}
public interface IRepository<TEntity> where TEntity : class
{
    Task<TEntity?> Single(Expression<Func<TEntity, bool>> predicate);

    IQueryable<TEntity> All();
    Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);

    void Add(TEntity entity);
    Task AddAsync(TEntity entity);
    void AddRange(IEnumerable<TEntity> entities);
    Task AddRangeAsync(IEnumerable<TEntity> entities);

    void Remove(TEntity entity);
    void RemoveRange(IEnumerable<TEntity> entities);
}