﻿using System.Net;
using System.Net.Sockets;
using Tbox.Core.Domains;
using Tbox.Core.Models;

namespace Tbox.Core.Repositories;

public sealed class FcmInfoRepository : Repository<FcmInfo>, IFcmInfoRepository
{
    private readonly ApplicationDbContext _db;
    private readonly ILogger _logger;

    public FcmInfoRepository(ApplicationDbContext db, ILogger logger)
        : base(db, logger)
    {
        _db = db;
        _logger = logger;
    }

    public async Task<int> GetRegisterId(int appId, string oneId)
    {
        var lastest = await _db.FcmInfos.Where(o => o.AppId == appId && o.OneId == oneId).OrderByDescending(o => o.UpdatedTime).FirstOrDefaultAsync();
        return lastest is not null
            ? lastest.RegisterId
            : -1;
    }

    public FcmInfo Add(FcmInfoRequestModel data)
    {
        var conn =_db.Database.GetConnectionString();
        var host = Dns.GetHostEntry(Dns.GetHostName());
        var address = host.AddressList.Where(ip => ip.AddressFamily == AddressFamily.InterNetwork).Select(ip => ip.ToString());
        var ip = string.Format("{0} ({1})", host.HostName, string.Join(", ", address));
        _logger.LogInformation($"{nameof(FcmInfo)}||Symfcmconnectionstring:{conn}||SymfcmIp:{ip}");
        if (_db.Database.GetDbConnection()== null || _db.Database.GetDbConnection().State!= System.Data.ConnectionState.Open)
        {
            _db.Database.GetDbConnection().Open();

        }

        var chk = _db.FcmInfos.FirstOrDefault(o => o.OneId == data.OneId && o.AppId == data.AppId && o.RegisterId == data.RegisterId);
        if (chk is null)
        {
            var added = _db.FcmInfos.Add(new FcmInfo
            {
                OneId = data.OneId,
                AppId = data.AppId,
                RegisterId = data.RegisterId,
                CreatedTime = DateTime.Now,
                UpdatedTime = DateTime.Now
            });
            _db.SaveChanges();
            _db.Database.GetDbConnection().Close();
            return added.Entity;
        }
        else
        {
            chk.UpdatedTime = DateTime.Now;
            _db.SaveChanges();

            return chk;
        }
    }
}

public interface IFcmInfoRepository
{
    FcmInfo Add(FcmInfoRequestModel data);
    Task<int> GetRegisterId(int appId, string oneId);
}