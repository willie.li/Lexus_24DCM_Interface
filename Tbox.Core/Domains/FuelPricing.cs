﻿namespace Tbox.Core.Domains;

public class FuelPricing
{
    [Key]
    public int Id { get; set; }
    public decimal Oil92 { get; set; }
    public decimal Oil95 { get; set; }
    public decimal Oil98 { get; set; }
    public decimal Diesel { get; set; }
    public DateTime ModifiedTime { get; set; }
}