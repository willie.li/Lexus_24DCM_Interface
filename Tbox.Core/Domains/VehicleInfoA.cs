﻿namespace Tbox.Core.Domains;

public sealed class VehicleInfoA
{
    [Key]
    [Unicode(false)]
    [MaxLength(20)]
    public string VIN { get; set; } = string.Empty;
    [Unicode(false)]
    [MaxLength(20)]
    public string? BDNO { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? CARCD { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? CARNM { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? TMCMDL { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? CARMDL { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? SFX { get; set; }
    public int MDLYEAR { get; set; }
    [Unicode(false)]
    [MaxLength(20)]
    public string? FRAN { get; set; }
    public DateTime CRTDT { get; set; }
    [Unicode(false)]
    [MaxLength(45)]
    public string? CRTPGMID { get; set; }
    public DateTime MTDT { get; set; }
    [Unicode(false)]
    [MaxLength(45)]
    public string? MTPGMID { get; set; }

    #region extra description
    /// <summary>
    /// 是否為國產車
    /// </summary>
    [NotMapped]
    public bool IsDomesticCar { get; set; }
    /// <summary>
    /// 是否為進口車
    /// </summary>
    [NotMapped]
    public bool IsForeignCar { get; set; }
    #endregion
}