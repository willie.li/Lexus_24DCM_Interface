﻿namespace Tbox.Core.Domains;

public sealed class FcmInfo
{
    [Key]
    public int Id { get; set; }
    [Unicode(false)]
    [MaxLength(36)]
    public string? OneId { get; set; }
    public int AppId { get; set; }
    public int RegisterId { get; set; }
    public DateTime CreatedTime { get; set; }
    public DateTime UpdatedTime { get; set; }
}