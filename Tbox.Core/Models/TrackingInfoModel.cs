﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public sealed record TrackingInfoModel
{
    [JsonPropertyName("id")]
    public string Id { get; set; } = string.Empty;
    [JsonPropertyName("vin")]
    public string Vin { get; set; } = string.Empty;
    [JsonPropertyName("trackid")]
    public int TrackId { get; set; }
    [JsonPropertyName("epoch")]
    public long Epoch { get; set; }
    [JsonPropertyName("lat")]
    public decimal Lat { get; set; }
    [JsonPropertyName("lon")]
    public decimal Lon { get; set; }
    [JsonPropertyName("speed")]
    public int Speed { get; set; }
    [JsonPropertyName("angle")]
    public int Angle { get; set; }
    [JsonPropertyName("vehicleStatus")]
    public int VehicleStatus { get; set; }
    public string VehicleStatusName { get; set; } = string.Empty;
    [JsonPropertyName("gpsStatus")]
    public int GpsStatus { get; set; }
    public string GpsStatusName { get; set; } = string.Empty;
}