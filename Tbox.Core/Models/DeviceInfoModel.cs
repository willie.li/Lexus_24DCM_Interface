﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public sealed record DeviceInfoModel
{
    [JsonPropertyName("expiryTimeEpoch")]
    public long ExpiryTimeEpoch { get; set; }
    [JsonPropertyName("deviceModel")]
    public string? DeviceModel { get; set; }
    [JsonPropertyName("imei")]
    public string? IMEI { get; set; }
    [JsonPropertyName("softwareVersion")]
    public string? SoftwareVersion { get; set; }
}