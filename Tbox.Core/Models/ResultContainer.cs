﻿namespace Tbox.Core.Models;

public sealed class ResultContainer<T>
{
    public string RequestId { get; } = Guid.NewGuid().ToString();
    /// <summary>
    /// T object as result
    /// </summary>
    public T? Result { get; set; }
    public double Duration { get; set; }
    public string RequestTime { get; } = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
}

public sealed class BaseOutputModel<T>
{
    public string RequestId { get; } = Guid.NewGuid().GetHashCode().ToString();
    public T? Data { get; set; }
    public long RequestTime { get; } = DateTime.Now.Ticks;
    /// <summary>
    /// 執行時間（ms）
    /// </summary>
    public double Durations { get; set; }
}