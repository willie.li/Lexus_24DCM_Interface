﻿namespace Tbox.Core.Models.RequestModels;

public class CreateFuelReportRequestModel
{
    public string VIN { get; set; }
    public long Epoch { get; set; }
    public double AvgFuelConsum { get; set; }
    public double FuelConsumption { get; set; }
    public double TravelDistance { get; set; }
    public double FuelDiff { get; set; }
    public int? Lat { get; set; } = 0;
    public int? Lon { get; set; } = 0;
}