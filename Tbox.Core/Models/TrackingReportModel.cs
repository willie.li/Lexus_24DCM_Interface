﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

public sealed record TrackingReportModel
{
    [JsonPropertyName("total")]
    public int Total { get; init; }
    [JsonPropertyName("reports")]
    public IEnumerable<TrackingData>? Reports { get; init; }
}

public sealed record TrackingData
{
    [JsonPropertyName("trackid")]
    public long TrackId { get; init; }
    [JsonPropertyName("epochStart")]
    public long EpochStart { get; init; }
    [JsonPropertyName("epochEnd")]
    public long EpochEnd { get; init; }
}