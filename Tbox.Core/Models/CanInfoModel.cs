﻿using System.Text.Json.Serialization;

namespace Tbox.Core.Models;

/// <summary>
/// CMX02:查詢車輛CAN狀態
/// </summary>
public sealed record CanInfoModel
{
    [JsonPropertyName("epoch")]
    public int Epoch { get; set; }
    [JsonPropertyName("totalMileage")]
    public int TotalMileage { get; set; }
    [JsonPropertyName("batteryVolt")]
    public double BatteryVolt { get; set; }
    [JsonPropertyName("tirePressureLF")]
    public int TirePressureLF { get; set; }
    [JsonPropertyName("tirePressureRF")]
    public int TirePressureRF { get; set; }
    [JsonPropertyName("tirePressureLB")]
    public int TirePressureLB { get; set; }
    [JsonPropertyName("tirePressureRB")]
    public int TirePressureRB { get; set; }

    [JsonPropertyName("startUpStatus")]
    public int StartUpStatus { get; set; }
    public string StartUpStatusName { get; set; }

    [JsonPropertyName("carDoorLF")]
    public int CarDoorLF { get; set; }
    [JsonPropertyName("carDoorRF")]
    public int CarDoorRF { get; set; }
    [JsonPropertyName("carDoorLB")]
    public int CarDoorLB { get; set; }
    [JsonPropertyName("carDoorRB")]
    public int CarDoorRB { get; set; }
    [JsonPropertyName("luggageDoor")]
    public int LuggageDoor { get; set; }
    [JsonPropertyName("hatchbackDoor")]
    public int HatchbackDoor { get; set; }
    public string CarDoorLFStatusName { get; set; } = string.Empty;
    public string CarDoorRFStatusName { get; set; } = string.Empty;
    public string CarDoorLBStatusName { get; set; } = string.Empty;
    public string CarDoorRBStatusName { get; set; } = string.Empty;
    public string LuggageDoorStatusName { get; set; } = string.Empty;
    public string HatchbackDoorStatusName { get; set; } = string.Empty;

    [JsonPropertyName("doorLockLF")]
    public int DoorLockLF { get; set; }
    [JsonPropertyName("doorLockRF")]
    public int DoorLockRF { get; set; }
    [JsonPropertyName("doorLockLB")]
    public int DoorLockLB { get; set; }
    [JsonPropertyName("doorLockRB")]
    public int DoorLockRB { get; set; }
    public string DoorLockLFStatusName { get; set; } = string.Empty;
    public string DoorLockRFStatusName { get; set; } = string.Empty;
    public string DoorLockLBStatusName { get; set; } = string.Empty;
    public string DoorLockRBStatusName { get; set; } = string.Empty;
    /// <summary>
    /// 引擎防盜鎖
    /// </summary>
    [JsonPropertyName("immobiliser")]
    public int ImMobiliser { get; set; }

    // === new added-in
    [JsonPropertyName("eot")]
    public int EOT { get; set; }
    [JsonPropertyName("ect")]
    public int ECT { get; set; }
    [JsonPropertyName("tot")]
    public int TOT { get; set; }
    [JsonPropertyName("eop")]
    public int EOP { get; set; }
    [JsonPropertyName("eol")]
    public int EOL { get; set; }
    [JsonPropertyName("cct")]
    public int CCT { get; set; }
    [JsonPropertyName("fl")]
    public int FL { get; set; }
}

public enum StartupStatusEnum
{
    AccOff = 1,
    AccOn = 2,
    IgnOn = 3
}

public enum CarDoorTypeEnum
{
    Open = 1,
    Close = 2
}

public enum DoorLockTypeEnum
{
    Lock = 1,
    Unlock = 2
}