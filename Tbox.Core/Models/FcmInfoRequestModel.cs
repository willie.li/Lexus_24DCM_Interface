﻿namespace Tbox.Core.Models;

public class FcmInfoRequestModel
{
    public string? OneId { get; set; }
    public int AppId { get; set; }
    public int RegisterId { get; set; }
}