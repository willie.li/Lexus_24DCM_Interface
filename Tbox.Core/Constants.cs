﻿namespace Tbox.Core;

public static class Constants
{
    // strings
    public const string TboxSqlConnectionStringKeyName = "TboxSQL";

    // Minimal api route definations
    public const string AppEndpointsPrefix = "/api/app";
    public const string CarInfoEndpointsPrefix = "/api/car";
    public const string VehicleInfoEndpointsPrefix = "/api/veh";
    public const string QueryEndpointsPrefix = "/api/query";

    // Minimal api endpoint tags
    public const string AppTagName = "1 App";
    public const string CarInfoTagName = "2 CarInfo";
    public const string VehicleInfoTagName = "3 VehicleInfo";
    public const string QueryTagName = "4 Query";
    public const string BackendManageTagName = "9 BackendManage";

    public const string GlobalHtFCMTokenKeyName = "HtFcmToken";

    public static class HeaderName
    {
        public const string REQUEST_TOKEN_KEY_NANE = "Token";
        public const string REQUEST_ONEID_KEY_NAME = "OneId";
        public const string REQUEST_ONEID_KEY_NAME2 = "RequestedOneId";
        public const string REQUEST_VIN_KEY_NAME = "RequestedVIN";

        public const string VALID_VIN_KEY_NAME = "ValidVIN";
        public const string VALID_ONEID_KEY_NAME = "ValidOneId";

        public const string REQUEST_ADMIN_NAME = "Admin";
        public const string REQUEST_SECRET_NAME = "Secret";
    }

    public const string REQUIRED_HEADERS_HINT = "<br/><br/>please provide <b>Token</b>, <b>RequestedVIN</b> from [Header]";

    public const string REQUEST_ADMIN_SECRET_KEY = "86315938-1742-49c2-8f5f-5be6f70651a9";
}