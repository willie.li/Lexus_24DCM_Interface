﻿using System.Security.Cryptography;
using System.Text;

namespace Tbox.Core.Crypto;

public class TripleDes : IDisposable
{
    private readonly TripleDES _3des;
    private bool _disposedValue1;

    public TripleDes(string key, string iv = "")
    {
        _3des = TripleDES.Create();

        _3des.KeySize = 192; // KEY length: 192/8 = 24
        _3des.Key = Encoding.UTF8.GetBytes(key);

        _3des.BlockSize = 64; // IV Length = 64/8 = 8;
        if (iv?.Length == 8)
            _3des.IV = Encoding.UTF8.GetBytes(iv);

        _3des.Mode = CipherMode.ECB;
        _3des.Padding = PaddingMode.PKCS7;
    }

    public string Encrypt(string plainText)
    {
        if (string.IsNullOrEmpty(plainText))
            throw new ArgumentNullException(nameof(plainText));

        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);

        using var encryptor = _3des.CreateEncryptor(_3des.Key, _3des.IV);
        var bytes = encryptor.TransformFinalBlock(plainTextBytes, 0, plainTextBytes.Length);
        return Convert.ToHexString(bytes);
    }

    public string Decrypt(string? source)
    {
        if (string.IsNullOrEmpty(source))
            throw new ArgumentNullException(nameof(source));

        var srcBytes = Convert.FromHexString(source);

        using var decryptor = _3des.CreateDecryptor(_3des.Key, _3des.IV);
        var bytes = decryptor.TransformFinalBlock(srcBytes, 0, srcBytes.Length);
        return Encoding.UTF8.GetString(bytes);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!_disposedValue1)
        {
            if (disposing)
            {
                _3des.Dispose();
            }
            _disposedValue1 = true;
        }
    }

    ~TripleDes()
    {
        Dispose(disposing: false);
    }

    public void Dispose()
    {
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}